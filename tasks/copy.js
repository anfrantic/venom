var gulp = require('gulp'),
    util = require('gulp-util'),
    notifier = require('../notify');

module.exports = function(source, destination) {
    return function() {
        var stream = gulp.src(source)
        
        .pipe(notifier.error())
        
        .pipe(gulp.dest(destination))

        .on('end', notifier.notify('Copying ' + source));

        return stream;
    }
}
