var gulp = require('gulp'),
    util = require('gulp-util'),
    imagemin = require('gulp-imagemin'),
    notifier = require('../notify');

module.exports = function(source, destination) {
    return function() {
        var stream = gulp.src(source)

        .pipe(notifier.error())

        .pipe(util.env.dev ? util.noop(): imagemin({
            progressive: true,
            optimizationLevel: 3
        }))
        
        .pipe(gulp.dest(destination))

        .on('end', notifier.notify('Processing images ' + source));

        return stream;
    }
}
