var gulp = require('gulp'),
    util = require('gulp-util'),
    jade = require('gulp-jade'),
    notifier = require('../notify');

module.exports = function(source,destination) {
    return function() {
        var stream = gulp.src(source)

        .pipe(notifier.error())

        .pipe(jade({
            pretty: true
        }))

        .pipe(gulp.dest(destination))

        .on('end', notifier.notify('Compiling jade ' + source));

        return stream;
    }
}
