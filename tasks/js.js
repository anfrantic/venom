var gulp = require('gulp'),
    util = require('gulp-util'),
    include = require('gulp-include'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    notifier = require('../notify');

module.exports = function(source, destination, filename) {
    return function() {
        var stream = gulp.src(source)

        .pipe(notifier.error())

        .pipe(include())

        .pipe(filename ? concat(filename) : util.noop())

        .pipe(util.env.dev ? util.noop() : uglify())

        .pipe(gulp.dest(destination))
        
        .on('end', notifier.notify('Compiling js ' + source));

        return stream;
    }
}
