var gulp = require('gulp'),
    util = require('gulp-util'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    notifier = require('../notify');

module.exports = function(source, destination, filename) {
    return function() {
        var stream = gulp.src(source)

        .pipe(notifier.error())

        .pipe(less())

        .pipe(util.env.dev ? util.noop() : autoprefixer('last 5 versions'))

        .pipe(filename ? concat(filename) : util.noop())

        .pipe(util.env.dev ? util.noop() : cssmin({
            keepSpecialComments: 1
        }))
        
        .pipe(gulp.dest(destination))

        .on('end', notifier.notify('Compiling less ' + source));

         return stream;
     }
}
