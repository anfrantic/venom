var gulp = require('gulp'),
    util = require('gulp-util'),
    bower = require('gulp-bower'),
    notifier = require('../notify');

module.exports = function(target, cmd) {
    return function() {
        var command = cmd || 'install';
        
        var stream =  bower({
            directory: target, 
            cmd: command
        })
        
        .pipe(notifier.error())

        .on('end', notifier.notify('Installing bower dependencies'));

        return stream;
    }
}
