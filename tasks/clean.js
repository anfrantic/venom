var gulp = require('gulp'),
    util = require('gulp-util'),
    rimraf = require('gulp-rimraf'),
    notifier = require('../notify');

module.exports = function(source) {
    return function() {
        var stream = gulp.src(source, {read: false})

        .pipe(notifier.error())

        .pipe(rimraf({
            force: true
        }))

        .on('end', notifier.notify('Deleting ' + source));

        return stream;
    }
}
