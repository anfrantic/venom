var notifire = require('node-notifier');
var plumber = require('gulp-plumber');

exports.message = function(message) {
    notifire.notify({
        title: 'Venom',
        icon: __dirname + '/assets/icon.png',
        message: message
    });
}

exports.notify = function (message) {
    return function() {
        notifire.notify({
            title: 'Venom',
            icon: __dirname + '/assets/icon.png',
            message: message
        });
    }
}

exports.error = function(message) {
    return plumber(function(error) {
        var notification = message || error.message;
        notifire.notify({
            title: 'Venom Error',
            icon: __dirname + '/assets/icon_error.png',
            message: error.message
        });

        this.emit('end');
    })
}