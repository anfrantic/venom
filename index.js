var gulp = require('gulp'),
    util = require('gulp-util'),
    watch = require('gulp-watch'),
    server = require('browser-sync').create(),
    plugins = require('require-dir')('./tasks');

// Create gulp venom wrapper
function Venom() {};

// Plugins container
Venom.prototype.do = plugins;

// Util environment
Venom.prototype.env = util.env;

// Environment checker
Venom.prototype.is = function(env) {
    return this.env[env] ? true: false;
};

// Run collback in specified environment
Venom.prototype.in = function(env, cb) {
    if (this.is(env)) {
        cb.call(this);
    }

    return this;
};

// Register gulp task
Venom.prototype.task = function() {
    gulp.task.apply(gulp, arguments);

    return this;
};

// Run gulp task
Venom.prototype.start = function() {
    gulp.start.apply(gulp, arguments);

    return this;
};

// Gulp file watcher replacement
Venom.prototype.watch = function(glob, tasks) {
    this.in('dev', function() {
        watch(glob, function() {
            this.start(tasks);
        });
    });
    
    return this;
};

// Browsersync server
Venom.prototype.sync = function(options) {
    this.in('dev', function(){
        server.init(options);
    });
    
    return this;
};

// Default tasks for specified environment
Venom.prototype.when = function(env, dep, cb) {
    this.in(env, function(){
        this.task('default', dep, cb);
    });
    
    return this;
};

// Default tasks
Venom.prototype.default = function(dep, cb) {
    if (typeof gulp.tasks['default'] === 'undefined') {
        this.task('default', dep, cb);
    }
    
    return this;
};

var inst = new Venom;
module.exports = inst;
